class ReviewsController < ApplicationController
	def create
		post = Post.find(params[:review][:post_id])
		user = User.find(params[:review][:user_id])
		rate = params[:review][:rate]
		body = params[:review][:body]

		@review = Review.new(rate: rate, body: body, post_id: post.id, user_id: user.id)

		if @review.save
			flash[:notice] = "Review created."
			redirect_to post_path(post)
		else
			flash[:notice] = "Review not created."
			redirect_to post_path(post)
		end
	end
end