class PostsController < ApplicationController
	before_action :authenticate_user!, only: [:new, :create, :destroy]

	def index
		@posts = Post.all.reverse	
	end

	def show
		@post = Post.find(params[:id])
	end

	def new	
		@user = current_user
	end

	def create
  	@user = current_user

  	if @user.posts.create(post_params)
  		redirect_to @user, notice: 'Photo was successfully created.'
  	else
  		redirect_to @user, notice: 'Smthng wrong.'
  	end
  end

  def destroy
  	post = Post.find(params[:id])
  	
  	if post.user == current_user
  		post.destroy		
			flash[:notice] = "Post was destroyed."
			redirect_to user_path(post.user)
		else
			flash[:notice] = "No access."
			redirect_to user_path(post.user)
		end
	end

	private

	def post_params
		params.require(:post).permit(:title, :image)		
	end
end