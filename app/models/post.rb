class Post < ApplicationRecord
  belongs_to :user
  has_many :reviews, dependent: :destroy

  mount_uploader :image, ImageUploader

  def score
  	score = 0
  	count = 0
  	self.reviews.each do |review|
  		score += review.rate
  		count += 1
  	end

  	return 0 if count == 0
  	average = score / count

  	return average  	
  end
end
